import { FETCH_LAYOUT_DATA, FETCH_LAYOUT_DATA_ERROR, FETCH_LAYOUT_DATA_LOADING } from '../constants'
import { apolloFetchNoAuth } from '../apolloFetchUtils'

export function layoutDataFetchData(fetchQuery, limit = 30, page = 1) {
    return (dispatch, getState) => {

        dispatch(layoutDataIsLoading(true))
        let query

        if (fetchQuery) {
            query = fetchQuery
        } else {
            query = `
            query allLayouts {
                layouts(limit: ${limit}, page: ${page}) {
                    data {
                        id
                        layout_name
                        layout_mode
                        layout_margins
                        width
                        height
                    }
                }
            }
            `
        }

        apolloFetchNoAuth({ query })
            .then((data) => {
                dispatch(layoutDataFetchDataSuccess(data.data.layouts.data))
                dispatch(layoutDataIsLoading(false))
                if (data.data.layouts.error) {
                    dispatch(layoutDataHasErrored(true))
                } else {
                    dispatch(layoutDataHasErrored(false))
                }
            })
            .catch((error) => {
                dispatch(layoutDataIsLoading(false))
                dispatch(layoutDataHasErrored(true))
            })
    }
}

export function layoutDataHasErrored(bool) {
    return {
        type: FETCH_LAYOUT_DATA_ERROR,
        hasErrored: bool
    }
}

export function layoutDataIsLoading(bool) {
    return {
        type: FETCH_LAYOUT_DATA_LOADING,
        isLoading: bool
    }
}

export function layoutDataFetchDataSuccess(layoutData) {
    return {
        type: FETCH_LAYOUT_DATA,
        layout: layoutData
    }
}
