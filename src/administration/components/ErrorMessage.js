import React,{Component} from 'react'
import { Message } from 'semantic-ui-react'

class ErrorMessage extends Component{
  render(){
    return(
      <Message negative hidden={this.props.hidden} onDismiss={this.props.onDismiss}>
        <Message.Header>{this.props.header}</Message.Header>
        <p>{this.props.message}</p>
      </Message>
    )
  }
}

export default ErrorMessage
