import React from 'react'
import { Table, Popup, Button, Icon, Confirm } from 'semantic-ui-react'

import { translate } from 'react-i18next'

import Can from '../../containers/Permission'

class UsersTable extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      selected: -1,
      openConfirm: false,
      openConfirmImpersonateUser: false,
      impersonateUserID: -1,
      userToRemove: -1
    }
  }

  // select the clicked user
  clicked(id){
    this.setState({selected: id},()=>{this.props.userSelected(id)})
  }

  // handle color change onclick in table
  colorRow(rowId){
    if(this.state.selected === rowId) return true
    else return false
  }

  // handle remove user from team
  handleRemove(){
    if(this.props.users.filter((el)=>{return el.user_id === this.state.userToRemove}).length > 1){
      this.props.removeUser(this.state.userToRemove)
    }else{
      this.props.removeUser(-1)
    }
    this.setState({openConfirm: false})
  }

  // close confirm delete user from team
  discard(){
    this.setState({openConfirm: false})
  }

  // call props modify user
  mod(id){
    this.props.modUser(id)
  }

  // open confirm delete user from team
  showConfirm(id){
    this.setState({
      openConfirm: true,
      userToRemove:id
    })
  }

  // handle openClose confirm impersonate user
  openCloseImpersonateUser(id, e) {
    this.setState({openConfirmImpersonateUser: !this.state.openConfirmImpersonateUser, impersonateUserID: id})
  }

  // MAIN RENDER
  render() {
    const { t } = this.props

    let loggedUserRoleIndex = 0,
        loggedUserRoleWeight = 0
    if (this.props.roles && this.props.userRoleID) {
      loggedUserRoleIndex = this.props.roles.findIndex(role => role.id === this.props.userRoleID)
      loggedUserRoleWeight = this.props.roles[loggedUserRoleIndex].weight
    }

    let rows = this.props.users.map((item,idx)=>{
      let itemRoleIndex = 0,
          itemRoleWeight = 0
      if (this.props.roles && this.props.userRoleID) {
        itemRoleIndex = this.props.roles.findIndex(role => role.id === item.role_id)
        itemRoleWeight = this.props.roles[itemRoleIndex].weight
      }

      return(
        <Table.Row
          key={idx}
          onClick={()=>{this.props.selectable ? this.clicked(item.user_id ? item.user_id : item.id) : null }}
          style={{cursor: 'pointer'}}
          positive={this.state.selected === (item.user_id ? item.user_id : item.id) ? true : false}
        >
          <Table.Cell>
            {item.user_id ? item.user_id : item.id}
          </Table.Cell>
          <Table.Cell>
            {item.name}
          </Table.Cell>
          <Table.Cell>
            {item.username ? item.username : item.user}
          </Table.Cell>
          <Table.Cell>
            {item.email}
          </Table.Cell>
          <Table.Cell>
            {item.organization}
          </Table.Cell>
          <Table.Cell>
            {item.web_site ? item.web_site : item.link_web}
          </Table.Cell>
          <Table.Cell hidden={!this.props.manageUsers}>
          {/* <Can perform='can_manage_team'>
            <Popup
              trigger={<Button circular icon={<Icon name='remove user' size='large'/>} onClick={()=>this.showConfirm(item.user_id ? item.user_id : item.id)}/>}
              content= {t("POPUP_DEL")}
              />
          </Can> */}
          <Can perform='can_manage_staff'>
            <Popup
              trigger={<Button circular icon={<Icon name='random' size='large'/>}
                onClick={this.openCloseImpersonateUser.bind(this, item.user_id)}
                disabled={item.user_id === this.props.loggedUserID ? true : false}
              />}
              content= "Impersonifica utente"
            />
          </Can>
          <Can perform='can_manage_user'>
            <Popup
              trigger={<Button circular icon={<Icon name='edit' size='large'/>} onClick={()=>{this.props.openEdit(item)}}
                disabled={itemRoleWeight > loggedUserRoleWeight ? true : false}
              />}
              content= "Modifica dati"
            />
          </Can>
          </Table.Cell>
        </Table.Row>
      )
    })

    return (
      <Table celled padded selectable={this.props.selectable}>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>ID</Table.HeaderCell>
            <Table.HeaderCell>Nome</Table.HeaderCell>
            <Table.HeaderCell>Username</Table.HeaderCell>
            <Table.HeaderCell>Email</Table.HeaderCell>
            <Table.HeaderCell>Organizzazione</Table.HeaderCell>
            <Table.HeaderCell>Sito Web</Table.HeaderCell>
            <Table.HeaderCell hidden={!this.props.manageUsers}>Azioni</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
      <Table.Body>
          {rows}
          <Confirm
            open={this.state.openConfirm}
            onCancel={this.discard.bind(this)}
            onConfirm={this.handleRemove.bind(this)}
            content="Sei sicuro di voler eliminare questo utente dal team?"
            header="Conferma azione"
          />
          <Confirm
            open={this.state.openConfirmImpersonateUser}
            onCancel={this.openCloseImpersonateUser.bind(this, -1)}
            onConfirm={() => this.props.takeControlUser(this.state.impersonateUserID)}
            content="Sei sicuro di voler impersonare questo utente?"
            header="Conferma azione"
          />
        </Table.Body>
      </Table>
    )
  }
}

export default translate('translations')(UsersTable)
