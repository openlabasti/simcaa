import {
  FETCH_USERTEAM_DATA,
  FETCH_MEMBERTEAM_DATA,
  FETCH_USERTEAM_DATA_ERROR,
  FETCH_USERTEAM_DATA_LOADING,
  FETCH_USERTEAM_DATA_COMPLETED
} from "../constants";
import { apolloFetchNoAuth } from '../apolloFetchUtils'
import { filterSetValue } from './FilterActions'

export function userTeamDataFetchData(fetchQuery, limit = 30, page = 1) {
    return (dispatch, getState) => {

        dispatch(userTeamDataIsLoading(true))
        dispatch(userTeamDataIsCompleted(false))

        let query
        if (fetchQuery) {
            query = fetchQuery
        } else {
            query = `
            query allTeamCurrentUser {
                user_teams (user_id: ${getState().user.id}, limit: ${limit}, page: ${page}){
                    data {
                      user_id
                      name
                      team_id
                      team_name
                    }
                }
            }
            `
        }

        apolloFetchNoAuth({ query })
            .then((data) => {
                dispatch(userTeamDataIsLoading(false))
                if (data.data.user_teams.error) {
                    dispatch(userTeamDataHasErrored(true))
                } else {
                    dispatch(userTeamDataHasErrored(false))
                    dispatch(userTeamDataFetchDataSuccess(data.data.user_teams.data))

                    // Set default team
                    dispatch(filterSetValue(data.data.user_teams.data[0].team_id))

                    // Set the finish of the query for InitProgress
                    dispatch(userTeamDataIsCompleted(true))
                }
            })
            .catch((error) => {
                dispatch(userTeamDataIsLoading(false))
                dispatch(userTeamDataHasErrored(true))
            })
    }
}

export function membersTeamDataFetchData(fetchQuery, id = null, limit = 1000, page = 1) {
    return (dispatch, getState) => {
        dispatch(userTeamDataIsLoading(true))
        let query,
        team_id = id ? id : getState().filterValue
        
        if (fetchQuery) {
            query = fetchQuery
        } else {
            query = `
            query allMembersCurrentTeam {
                user_teams (team_id: ${team_id}, limit: ${limit}, page: ${page}){
                    data {
                      user_id
                      username
                      team_id
                    }
                }
            }
            `
        }
        
        apolloFetchNoAuth({ query })
        .then((data) => {
            dispatch(userTeamDataIsLoading(false))
            if (data.data.user_teams.error) {
                dispatch(userTeamDataHasErrored(true))
            } else {
                dispatch(userTeamDataHasErrored(false))
                dispatch(membersTeamDataFetchDataSuccess(data.data.user_teams.data))
            }
        })
        .catch((error) => {
            dispatch(userTeamDataIsLoading(false))
            dispatch(userTeamDataHasErrored(true))
        })
    }
}

export function userTeamDataHasErrored(bool) {
    return {
        type: FETCH_USERTEAM_DATA_ERROR,
        hasErrored: bool
    }
}

export function userTeamDataIsLoading(bool) {
    return {
        type: FETCH_USERTEAM_DATA_LOADING,
        isLoading: bool
    }
}

export function userTeamDataIsCompleted(isCompleted) {
    return {
        type: FETCH_USERTEAM_DATA_COMPLETED,
        isCompleted
    }
}

export function userTeamDataFetchDataSuccess(team) {
    return {
        type: FETCH_USERTEAM_DATA,
        team
    }
}

export function membersTeamDataFetchDataSuccess(members) {
    return {
        type: FETCH_MEMBERTEAM_DATA,
        members
    }
}
