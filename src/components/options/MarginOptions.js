import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Dropdown, Input, Radio } from 'semantic-ui-react'
import { translate } from 'react-i18next'

import { isValidJSON } from '../../store/functionUtils'

class MarginOptions extends Component {
    constructor(props) {
        super(props)
        this.state = {
            layout: JSON.stringify(props.layout),
            optionsLayouts: [],
            maxMargin: 1000,
        }
    }

    componentDidMount() {
        let localMargins, localLayout = this.props.optionsLayouts

        if (!localLayout[0].hasOwnProperty('value') && !localLayout[0].hasOwnProperty('text')) {
                localLayout = localLayout.map((item, index) => {
                    let singleLayout = {}
                    singleLayout.key = index
                    singleLayout.text = item.layout_name
                    singleLayout.value = JSON.stringify(item)
                    localMargins = item.layout_margins
                    return singleLayout
                })
                this.setState({optionsLayouts: localLayout, lineSpacing: localMargins})
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.layout !== this.state.layout) {
            let localLayout = JSON.parse(this.state.layout)
            if (isValidJSON(localLayout.layout_margins)) {
                localLayout.layout_margins = JSON.parse(localLayout.layout_margins)
                this.props.updateLayout(localLayout)
            } else {
                this.props.updateLayout(localLayout)
            }
        }
    }

    // Handel change mode of the layout (0 = portrait, 1 = landscape)
    handleChangeMode(e, data) {
        let localLayout = Object.assign({}, JSON.parse(this.state.layout))
        localLayout.layout_mode = data.value
        let max = Math.max(localLayout.width, localLayout.height)
        let min = Math.min(localLayout.width, localLayout.height)
        if (data.value === 0) {
            localLayout.width = min
            localLayout.height = max
        } else {
            localLayout.width = max
            localLayout.height = min
        }
        this.setState({layout: JSON.stringify(localLayout)})
    }

    // Handle dropdown layout change
    handleDropdownChange(e, data) {
        this.setState({layout: data.value})
    }

    // Handle change new line/paragraph
    handleChangeMargins(type, e, data) {
        let margin = data === undefined || data === null ? parseInt(e.target.value, 10) : parseInt(data.value, 10)
        let localLayout = JSON.parse(this.state.layout)
        if (!isNaN(margin) && margin >= 0 && margin <= 1000) {
            if (isValidJSON(localLayout.layout_margins)) {
                localLayout.layout_margins = JSON.parse(localLayout.layout_margins)
            }
            localLayout.layout_margins[type] = Math.floor(margin)
        } else if (!isNaN(margin) && margin < 0) {
            if (isValidJSON(localLayout.layout_margins)) {
                localLayout.layout_margins = JSON.parse(localLayout.layout_margins)
            }
            localLayout.layout_margins[type] = 0
        } else if (!isNaN(margin) && margin > 1000) {
            if (isValidJSON(localLayout.layout_margins)) {
                localLayout.layout_margins = JSON.parse(localLayout.layout_margins)
            }
            localLayout.layout_margins[type] = 1000
        }
        this.setState({layout: JSON.stringify(localLayout)})
    }

    // Parse margins for input
    parseMargin() {
        let localMargin = JSON.parse(this.state.layout).layout_margins
        if (localMargin !== null && typeof localMargin === 'object') {
            return localMargin
        } else if (isValidJSON(localMargin)) {
            localMargin = JSON.parse(localMargin)
            return localMargin
        }
    }

    // MAIN RENDER
    render() {
        const { t } = this.props

        if (this.state.optionsLayouts.length === 0) {
            return <div></div>
        }

        let layoutIndex = this.state.optionsLayouts.map(e => JSON.parse(e.value).layout_name).indexOf(this.props.layout.layout_name)
        let localMargin = this.parseMargin()
        return (
            <Form>
                <Form.Field width={8}>
                    <label>{t("MAIN_FRM_LAYOUT")}</label>
                    <Dropdown selection options={this.state.optionsLayouts}
                        placeholder={t("MAIN_FRM_PLACEHOLDER_LAYOUT")}
                        defaultValue={this.state.optionsLayouts.length > 0 ? this.state.optionsLayouts[layoutIndex].value : null}
                        onChange={this.handleDropdownChange.bind(this)}
                    />
                </Form.Field>
                <Form.Field>
                    <Radio
                        label={this.props.t("OPT_LBL_PORTRAIT")}
                        value={0}
                        checked={parseInt(JSON.parse(this.state.layout).layout_mode) === 0 ? true : false}
                        onChange={this.handleChangeMode.bind(this)}
                    />
                </Form.Field>
                <Form.Field>
                    <Radio
                        label={this.props.t("OPT_LBL_LANDSCAPE")}
                        value={1}
                        checked={parseInt(JSON.parse(this.state.layout).layout_mode) === 1 ? true : false}
                        onChange={this.handleChangeMode.bind(this)}
                    />
                </Form.Field>
                <Form.Group widths='equal'>
                    <Form.Field>
                        <Form.Group widths='equal'>
                            <Form.Field>
                                <label>{t("OPT_LBL_MARGINTOP")}</label>
                                <input type="number" min="0" max={this.state.maxMargin}
                                    onChange={this.handleChangeMargins.bind(this, 'top')}
                                    value={localMargin.top}
                                />
                            </Form.Field>
                            <Form.Field>
                                <label>{t("OPT_LBL_MARGINLEFT")}</label>
                                <input type="number" min="0" max={this.state.maxMargin}
                                    onChange={this.handleChangeMargins.bind(this, 'left')}
                                    value={localMargin.left}
                                />
                            </Form.Field>
                            <Form.Field>
                                <label>{t("OPT_LBL_MARGINBOTTOM")}</label>
                                <input type="number" min="0" max={this.state.maxMargin}
                                    onChange={this.handleChangeMargins.bind(this, 'bottom')}
                                    value={localMargin.bottom}
                                />
                            </Form.Field>
                            <Form.Field>
                                <label>{t("OPT_LBL_MARGINRIGHT")}</label>
                                <input type="number" min="0" max={this.state.maxMargin}
                                    onChange={this.handleChangeMargins.bind(this, 'right')}
                                    value={localMargin.right}
                                />
                            </Form.Field>
                        </Form.Group>
                    </Form.Field>
                    <Form.Field>
                        <Form.Group widths='equal'>
                            <Form.Field>
                                <label>{t("OPT_LBL_LINESPACINGTOP")}</label>
                                <input type="number" min="0" max={this.state.maxMargin}
                                    onChange={this.handleChangeMargins.bind(this, 'line_spacing_top')}
                                    value={localMargin.line_spacing_top}
                                />
                            </Form.Field>
                            <Form.Field>
                                <label>{t("OPT_LBL_LINESPACINGLEFT")}</label>
                                <input type="number" min="0" max={this.state.maxMargin}
                                    onChange={this.handleChangeMargins.bind(this, 'line_spacing_left')}
                                    value={localMargin.line_spacing_left}
                                />
                            </Form.Field>
                        </Form.Group>
                    </Form.Field>
                </Form.Group>
                <Form.Group widths='equal'>
                    <Form.Field>
                        <label>{t("OPT_LBL_NEWLINE")}</label>
                        <input type="number" min="0" max={this.state.maxMargin}
                            onChange={this.handleChangeMargins.bind(this, 'new_line')}
                            value={localMargin.new_line}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>{t("OPT_LBL_PARAGRAPH")}</label>
                        <input type="number" min="0" max={this.state.maxMargin}
                            value={localMargin.new_paragraph}
                            onChange={this.handleChangeMargins.bind(this, 'new_paragraph')}
                        />
                    </Form.Field>
                </Form.Group>
            </Form>
        )
    }
}

MarginOptions.propTypes = {
    optionsLayouts: PropTypes.array.isRequired,
    layout: PropTypes.object.isRequired,
    updateLayout: PropTypes.func.isRequired,
}

export default translate('translations')(MarginOptions)
