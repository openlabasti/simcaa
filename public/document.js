window.homepage_document = {
    "box1_title": "PER I PRIVATI",
    "box1_icon": "info",
    "box2_title": "PER LE SCUOLE",
    "box2_icon": "info",
    "box3_title": "PER I PROFESSIONISTI",
    "box3_icon": "info",
    "box4_title": "TUTORIAL E DOCUMENTAZIONE",
    "box4_icon": "info",
    "homepage_message": `<h2>
        <p>OpenLab Asti - SimCAA</p>
        </h2>    
        <h3>
            <p>Stai usando l'applicazione SIMCAA "Scrittura Inclusiva Multimodale Comunicazione Aumentativa Aperta".</p>
            <p>Consigliamo l'utilizzo dei Browser Chrome o Chromium per una gestione più efficace di SIMCAA".</p>
        </h3>`,
    "conditions": "contidions",
    "gdpr": `
        <b>The standard Lorem Ipsum passage, used since the 1500s</b>
        <p />
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 
        ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
        voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat 
        non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        <p />
        <b>Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</b>
        <br />
        "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque 
        laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi 
        architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas 
        sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione 
        voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, 
        consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore 
        magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam 
        corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure 
        reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem 
        eum fugiat quo voluptas nulla pariatur?"
    `
}

window.login_document = {
    "policy": `
        <b>Benvenuto su SIMCAA!Benvenuto su SIMCAA!</b>
        <p />
        SIMCAA è una applicazione web sviluppata da OpenLab Asti.
        Utilizzando i nostri Servizi, l’utente accetta i presenti termini. Si prega di leggerli con attenzione.
        <p />
        SIMCAA utilizza il sistema simbolico ARASAAC <b>Autore simboli: Sergio Palao Proprietà: ARASAAC (http://catedu.es/arasaac/) Licenza: CC (BY-NC-SA)</b>, le opere prodotte con SIMCAA sono assogettate alla stessa licenza e condizioni d'uso.
        <p />
        <b>Utilizzo di SIMCAA</b>
        <br />
        Per utilizzare SIMCAA è necessario un account, rilasciato da un amministratore. Per proteggere l’account SIMCAA è necessario mantenere la password riservata. L’utente è responsabile dell’attività svolta su o attraverso il proprio account SIMCAA.  
        <p />
        Il servizo SIMCAA non deve essere utilizzato in modo improprio. 
        È possibile utilizzare il nostro Servizio solo nei modi consentiti dalla legge. 
        <p />
        Potremmo sospendere o interrompere l'accesso a SIMCAA all’utente qualora questi non rispettasse i nostri termini o le nostre norme.
        <p />
        SIMCAA consente di caricare, trasmettere, inviare o ricevere contenuti.
        Quando l'utente carica, trasmette, memorizza in SIMCAA, invia o riceve contenuti da o tramite SIMCAA, si impegna, sotto la propria responsabilità a:
        <p />
        <b>Non utilizzare/diffondere materiali</b> (disegni, fotografie, immagini) di proprietà di terzi lesivi dei diritti di copyright.
        Rispondiamo alle notifiche di presunta violazione del copyright provvedendo alla richiesta di rimozione e, in caso di recidiva alla chiusura dell'account dei trasgressore.
        <p />
        <b>Non rendere pubblici materiali</b> (disegni, fotografie, immagini) che coinvolgano il diritto alla privacy delle persone, ovvero senza formale autorizzazione. Un uso improprio di questa regola comporterà la rimozione dell'account.
        <p />
        <b>Informazioni sul software e sul servizio SIMCAA</b>
        <br />
        SIMCAA è un software opensource rilasciato con licenza AGPL. Il servizio è concesso gratuitamente senza obblighi o limitazioni fatta eccezione delle implicazioni tecniche ed infrastrutturali
        <p />
        OpenLab Asti non rilascia specifiche garanzie in relazione al Servizio. 
        Ad esempio, non rilascia alcuna garanzia sui contenuti del Servizio, sulla specifica funzione dei Servizio e sulla sua affidabilità, disponibilità o capacità di soddisfare le esigenze dell’utente. I Servizi sono forniti «così come sono».
        <p />
        L'utilizzo di SIMCAA è subordinato alla lettura ed alla accettazione delle suddette condizioni
    `,
}
