import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Menu, Dropdown } from 'semantic-ui-react'
import { translate } from 'react-i18next'
import { Link, withRouter } from 'react-router-dom'

import UploadSymbol from '../../containers/UploadSymbol'
import LanguageSwitcher from '../../LanguageSwitcher'
import NewProfileForm from '../../containers/homepage/NewProfileForm'
import UserProfile from '../../containers/homepage/UserProfile'
import TeamSwitcher from '../../containers/switcher/TeamSwitcher'
import Can from '../../containers/Permission'

class HomepageProject extends Component {
    constructor(props) {
        super(props)
        this.beforeUnloadFunction = this.beforeUnloadFunction.bind(this)
    }

    //  Logout...cancella il jwt dal session storage e redirige sulla login
    Logout() {
        window.removeEventListener("beforeunload", this.beforeUnloadFunction)
        sessionStorage.removeItem('state')
        sessionStorage.removeItem('jwt')
        setTimeout(() => {
            this.props.resetStore()
        }, 100)
        
        this.props.history.push('/')
    }

    componentDidMount() {
        // TODO: Da attivare in fase finale per evitare refresh o chiusura della scheda del browser
        window.addEventListener("beforeunload", this.beforeUnloadFunction)
    }

    beforeUnloadFunction(e) {
        e.preventDefault()
        e.returnValue = ''
    }

    render() {
        const { t } = this.props

        return (
            <Menu>
                <Menu.Menu position='right'>
                    <LanguageSwitcher type='dropdown' />
                    <Dropdown item text={t("HOME_NAVBAR_MANAGE")}>
                        <Dropdown.Menu>
                            <Can perform='can_upload_symbol' or='can_manage_preload'>
                                <UploadSymbol type='dropdown' user={this.props.user} buttonActionTranslateText='UPSY_UPLOAD' mode='upload' />
                            </Can>
                            <Can perform='can_manage_profile' or='can_create_profile'>
                                <NewProfileForm
                                    className='icon-pointer'
                                    size='big'
                                    type='dropdown'
                                />
                            </Can>
                            <Dropdown.Item as={Link} to="/preload">{t("HOME_NAVBAR_MANAGE_PRELOAD")}</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                    <TeamSwitcher type='menu' />
                    <Dropdown item text={this.props.user.user}>
                        <Dropdown.Menu>
                            <Can perform='can_access_admin'>
                                <Dropdown.Item as={Link} to="/admin">{t("HOME_NAVBAR_ADMINPANEL")}</Dropdown.Item>
                            </Can>
                            <UserProfile/>
                            <Dropdown.Item onClick={this.Logout.bind(this)}>{t("HOME_NAVBAR_USER_LOGOUT")}</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </Menu.Menu>
            </Menu>
        )
    }
}

HomepageProject.propTypes = {
    user: PropTypes.object.isRequired,
    resetStore: PropTypes.func
}

export default translate('translations')(withRouter(HomepageProject))
