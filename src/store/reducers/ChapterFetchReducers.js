import { FETCH_CHAPTER_DATA, FETCH_CHAPTER_DATA_CONTENT, FETCH_CHAPTER_DATA_ROW, FETCH_CHAPTER_DATA_TYPO, FETCH_CHAPTER_DATA_ERROR, FETCH_CHAPTER_DATA_LOADING } from '../constants'

export function chapterDataHasErrored(state = false, action) {
    switch (action.type) {
        case FETCH_CHAPTER_DATA_ERROR:
            return action.hasErrored

        default:
            return state
    }
}

export function chapterDataIsLoading(state = false, action) {
    switch (action.type) {
        case FETCH_CHAPTER_DATA_LOADING:
            return action.isLoading

        default:
            return state
    }
}

export function chapterData(state = [], action) {
    switch (action.type) {
        case FETCH_CHAPTER_DATA:
            return action.chapter

        default:
            return state
    }
}

export function chapterContentData(state = [], action) {
    switch (action.type) {
        case FETCH_CHAPTER_DATA_CONTENT:
            return action.content

        default:
            return state
    }
}

export function chapterRowsData(state = [], action) {
    switch (action.type) {
        case FETCH_CHAPTER_DATA_ROW:
            return action.rows

        default:
            return state
    }
}

export function chapterTypoData(state = [], action) {
    switch (action.type) {
        case FETCH_CHAPTER_DATA_TYPO:
            return action.typo

        default:
            return state
    }
}
