import {apolloFetchNoAuth} from '../apolloFetchUtils'
import { Permission } from '../../containers/Permission'

export function closeModal() {
    return (dispatch) => {
        dispatch({
            type: "HIDE_MODAL"
        })
        dispatch({
          type: "SAVE_ADMIN_USER_TO_ADD",
          user_to_add: []
        })
        dispatch({
          type: "SAVE_ADMIN_USER_TEAMS",
          user_teams: []
        })
    }
}
export function openModalCreateUser(team_id=-1) {
  if(team_id > -1){
    return (dispatch) => {
        dispatch({
            type: "OPEN_MODAL_CREATE_USR",
            toTeam: team_id
        })
    }
  }
}
export function openModalAddUser(team_id=-1) {
  if(team_id > -1){
    return (dispatch) => {
        dispatch({
            type: "OPEN_MODAL_ADD_USR",
            toTeam: team_id
        })
    }
  }
}
export function openModalManageTeam(team_id=-1) {
  if(team_id > -1){
    return (dispatch) => {
        dispatch({
            type: "OPEN_MODAL_MANAGE_TEAM",
            toTeam: team_id
        })
    }
  }
}
export function openModalEditUserParams(toUser=null) {
  if(toUser !== null){
    return (dispatch) => {
        dispatch({
            type: "OPEN_MODAL_EDIT_USER_PARAMS",
            toUser: toUser
        })
    }
  }
}
export function openModalCreateTeam() {
  return (dispatch) => {
    dispatch({
        type: "OPEN_MODAL_CREATE_TEAM"
    })
  }
}

export function saveRoles(){
  return(dispatch)=>{
    let query = `
      query getRoles{
        roles{
          data {
            id
            role_desc
            role_permissions
            weight
          }
        }
      }
    `
    apolloFetchNoAuth({query})
    .then((data)=>{
      let roles = data.data.roles.data.sort((a, b) => b.weight - a.weight)
      dispatch({
        type: "SAVE_ADMIN_ROLES",
        roles
      })
    })
  }
}
export function saveTeams(user_id = null, staff_id = null){
  return(dispatch, getState)=>{
    let query
    if (Permission(true, getState().user.user_permissions['can_manage_team'])) {
      query = `
        query getTeams{
          team(idstaff: ${staff_id || getState().user.idstaff}) {
            data {
              id
              name
              email
              idstaff
            }
          }
        }
      `
    } else {
      query = `
        query getTeams{
          user_teams(user_id: ${user_id || getState().user.id}) {
            data {
              team_id
              team_name
              team_email
              idstaff
            }
          }
        }
      `
    }
    apolloFetchNoAuth({query})
    .then((data)=>{
      let teamData
      if (data.data.team) {
        teamData = data.data.team.data
        teamData = teamData.map(item => {
          item.team_id = item.id
          item.team_name = item.name
          item.team_email = item.email
          delete item.id
          delete item.name
          delete item.email
          return item
        })
      } else {
        teamData = data.data.user_teams.data
      }
      dispatch({
        type: "SAVE_ADMIN_TEAMS",
        teams: teamData
      })
    })
  }
}
export function saveUserTeams(team_id){
  return(dispatch, getState)=>{
    let query = `
      query getUserTeams{
        user_teams(staff_id: ${getState().user.idstaff}, team_id: ${team_id}, limit: 100) {
          data{
            userteam_id
            user_id
            name
            username
            email
            user_permissions
            organization
            web_site
            status
            role_id
            idstaff
          }
        }
      }
    `
    apolloFetchNoAuth({query})
    .then((data)=>{
      data.data.user_teams.data.map((el)=>{
        el.user_permissions = JSON.parse(el.user_permissions)
      })
      dispatch({
        type: "SAVE_ADMIN_USER_TEAMS",
        user_teams: data.data.user_teams.data
      })
    })
  }
}
export function saveUserToAdd(staff_id, notInTeam){
  return(dispatch)=>{
    let data = {team_id: notInTeam, staff_id}

    fetch(window.env.RestApiNotInTeam, {
      method: 'POST',
      body: JSON.stringify(data),
      headers:{
        'Content-Type': 'application/json'
      }
    })
    .then(res => res.json())
    .then(data => {
      dispatch({
            type: "SAVE_ADMIN_USER_TO_ADD",
            user_to_add: data
          })
    })
    .catch(error => console.error(error))
  }
}
