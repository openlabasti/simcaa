import React, { Component, Fragment } from 'react'
import { Modal, Icon, Button, Input, Dropdown, List, Segment, Accordion, Form, Label, Message } from 'semantic-ui-react'
import { translate } from 'react-i18next'
import { connect } from 'react-redux'

import { apolloFetch } from '../../store/apolloFetchUtils'
import { userFetchData } from '../../store/actions/UserActions'

class UserProfile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modalOpen: false,
            accordionOpen: 1,
            successMessage: true,
            edit: '',
            oldPassword: '',
            newPassword: '',
            repeatPassword: '',
            name: props.user.name,
            email: props.user.email,
            organization: props.user.organization,
            link_web: props.user.link_web,
            error: {hidden: true, header: 'Errore', content: 'test'},
            userVariable: [{key: 'name', translate: 'TBL_NAME'}, {key: 'email', translate: 'TBL_EMAIL'},
                        {key: 'organization', translate: 'TBL_ORG'}, {key: 'link_web', translate: 'TBL_LWEB'}]
        }
    }

    // Handle trigger edit
    handleEdit(type, e) {
        if (type && this.state.edit === '') {
            this.setState({edit: type, [type]: this.props.user[type]})
        } else {
            this.setState({edit: ''})
        }
    }

    // Handle all input change
    handleInput(type, e) {
        this.setState({[type]: e.target.value})
    }

    // Handle open/close modal
    openCloseModal() {
        if (this.state.modalOpen) {
            this.setState({modalOpen: !this.state.modalOpen, accordionOpen: 1, edit: '',
                        oldPassword: '', newPassword: '', repeatPassword: '', successMessage: true,
                        error: {hidden: true, header: 'Errore', content: 'test'}
                    })            
        } else {
            this.setState({modalOpen: !this.state.modalOpen})
        }
    }

    // Update the user normal data
    updateUser(data, e) {
        let query = `
            mutation UpdateUser {
                updateCaaUser(id: ${this.props.user.id}, ${[data]}: "${this.state[data]}"){
                    id
                }
            }
        `
        apolloFetch({query})
            .then(data => {
                this.props.updateUserInfo(false, false)
                this.handleEdit(data)
            })
            .catch(error => console.log(error))
    }

    // Update password user
    updatePassword() {
        let localError = Object.assign({}, this.state.error)
        if (this.state.oldPassword !== '' && this.state.newPassword !== '' && this.state.repeatPassword !== '') {
            if (this.state.newPassword === this.state.repeatPassword) {
                let query = `
                    mutation UpdateUser {
                        updateCaaUser(id: ${this.props.user.id}, old_password: "${this.state.oldPassword}", new_password: "${this.state.newPassword}"){
                            id
                        }
                    }
                `
                apolloFetch({query})
                    .then(data => {
                        if (data.data.updateCaaUser.id !== -1) {
                            localError.hidden = true
                            this.setState({error: localError, successMessage: !this.state.successMessage})
                        } else {
                            localError.content = 'La vecchia password inserire è errata'
                            localError.hidden = false
                            this.setState({error: localError, successMessage: true}) 
                        }
                    })
                    .catch(error => console.log(error))
            } else {
                localError.content = 'La nuova password non coincide con la sua ripetizione'
                localError.hidden = false
                this.setState({error: localError, successMessage: true})
            }
        } else {
            localError.content = 'Riempi tutti i campi delle password'
            localError.hidden = false
            this.setState({error: localError, successMessage: true})
        }
    }

    // MAIN RENDER
    render() {
        const { t } = this.props

        let labelPassword
        if (this.state.newPassword !== '' || this.state.repeatPassword !== '') {
          if (this.state.newPassword === this.state.repeatPassword) {
            labelPassword = <Label color='green' pointing='left'>Le password inserite coincidono!</Label>          
          } else {
            labelPassword = <Label color='red' pointing='left'>Le password inserite non coincidono!</Label>
          }
        }

        return (
            <Modal closeOnDimmerClick={false}
                trigger={<Dropdown.Item onClick={this.openCloseModal.bind(this)}>{t("HOME_NAVBAR_USER_PROFILE")}</Dropdown.Item>}
                open={this.state.modalOpen}
            >
                <Modal.Header>Profilo di {this.props.user.name}</Modal.Header>
                <Modal.Content>
                    <Segment textAlign='center' basic>
                        <Icon name='user' size='massive' style={{textAlign: 'center'}} />
                    </Segment>
                    <List divided relaxed>
                        <List.Item header={t("TBL_USR")} content={this.props.user.user} />
                        {
                            this.state.userVariable.map((item, index) => {
                                return (
                                    <List.Item key={index}>
                                        <List.Header>{t(item.translate)}</List.Header>
                                        <List.Content>
                                            {
                                                this.state.edit === item.key ?
                                                <Fragment>
                                                    <Input value={this.state[item.key]} onChange={this.handleInput.bind(this, item.key)} />
                                                    <div style={{paddingLeft: '10px', display: 'inline'}}>
                                                        <Icon name='check' color='green' circular inverted onClick={this.updateUser.bind(this, item.key)} className='icon-pointer' />
                                                        <Icon name='cancel' color='red' circular inverted onClick={this.handleEdit.bind(this, item.key)} className='icon-pointer' />
                                                    </div>
                                                </Fragment>
                                                : <div style={{display: 'inline'}}>
                                                    {this.props.user[item.key]}
                                                    <Icon name='edit' size='large' style={{paddingLeft: '10px'}}
                                                        className='icon-pointer'
                                                        onClick={this.handleEdit.bind(this, item.key)}
                                                        disabled={this.state.edit !== '' ? true : false}
                                                    />
                                                </div>
                                            }
                                        </List.Content>
                                    </List.Item>
                                )
                            })
                        }
                        <List.Item header={t("TBL_GROUP")} content={this.props.user.role_id} />
                    </List>
                    <Accordion styled style={{width: '100%'}}>
                        <Accordion.Title active={this.state.accordionOpen === 0} index={0} onClick={() => this.setState({accordionOpen: 1-this.state.accordionOpen})}>
                            <Icon name='dropdown' />
                            {t("MAIN_PROFILE_PWDCHANGE")}
                        </Accordion.Title>
                        <Accordion.Content active={this.state.accordionOpen === 0}>
                        <Form>
                            <Form.Field width={16}>
                                <label>{t("MAIN_PROFILE_PWDOLD")}</label>
                                <input placeholder={t("MAIN_PROFILE_PWDOLD")} value={this.state.oldPassword}
                                    onChange={this.handleInput.bind(this, 'oldPassword')}
                                    type='password' style={{width: '50%'}}
                                />
                                {
                                    (this.state.newPassword !== '' || this.state.repeatPassword !== '') && this.state.oldPassword === '' ?
                                    <Label color='red' pointing='left'>Devi inserire anche la tua vecchia password!</Label>
                                    : null
                                }
                            </Form.Field>
                            <Form.Field width={16}>
                                <label>{t("MAIN_PROFILE_PWDNEW")}</label>
                                <input placeholder={t("MAIN_PROFILE_PWDNEW")} value={this.state.newPassword}
                                    onChange={this.handleInput.bind(this, 'newPassword')}
                                    type='password' style={{width: '50%'}}
                                />
                                {labelPassword}
                            </Form.Field>
                            <Form.Field width={16}>
                                <label>{t("MAIN_PROFILE_PWDREPEAT")}</label>
                                <input placeholder={t("MAIN_PROFILE_PWDREPEAT")} value={this.state.repeatPassword}
                                    onChange={this.handleInput.bind(this, 'repeatPassword')}
                                    type='password' style={{width: '50%'}}
                                />
                                {labelPassword}
                            </Form.Field>
                            <Button positive onClick={this.updatePassword.bind(this)}>{t("MAIN_PROFILE_PWDUPDATE")}</Button>
                        </Form>
                        <Message hidden={this.state.error.hidden} negative icon='exclamation circle'
                            header={this.state.error.header} content={this.state.error.content}
                        />
                        <Message hidden={this.state.successMessage} positive icon='check'
                            header={'Successo!!!'} content={'password cambiata correttamente'}
                        />
                        </Accordion.Content>
                    </Accordion>
                </Modal.Content>
                <Modal.Actions>
                    <Button negative onClick={this.openCloseModal.bind(this)}>{t("HEAD_BTN_CLOSE")}</Button>
                </Modal.Actions>
            </Modal>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateUserInfo: (noInit, isLogin)=> dispatch(userFetchData(noInit, isLogin))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(translate('translations')(UserProfile))