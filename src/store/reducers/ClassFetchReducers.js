import { FETCH_CLASS_DATA, FETCH_CLASS_DATA_ERROR, FETCH_CLASS_DATA_LOADING } from '../constants'

export function classDataHasErrored(state = false, action) {
    switch (action.type) {
        case FETCH_CLASS_DATA_ERROR:
            return action.hasErrored

        default:
            return state
    }
}

export function classDataIsLoading(state = false, action) {
    switch (action.type) {
        case FETCH_CLASS_DATA_LOADING:
            return action.isLoading

        default:
            return state
    }
}

export function classData(state = [], action) {
    switch (action.type) {
        case FETCH_CLASS_DATA:
            return action.class

        default:
            return state
    }
}
