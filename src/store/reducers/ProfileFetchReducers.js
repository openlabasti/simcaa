import { FETCH_PROFILE_DATA, FETCH_PROFILE_DATA_ERROR, FETCH_PROFILE_DATA_LOADING } from '../constants'

export function profileDataHasErrored(state = false, action) {
    switch (action.type) {
        case FETCH_PROFILE_DATA_ERROR:
            return action.hasErrored

        default:
            return state
    }
}

export function profileDataIsLoading(state = false, action) {
    switch (action.type) {
        case FETCH_PROFILE_DATA_LOADING:
            return action.isLoading

        default:
            return state
    }
}

export function profileData(state = [], action) {
    switch (action.type) {
        case FETCH_PROFILE_DATA:
            return action.profile

        default:
            return state
    }
}
