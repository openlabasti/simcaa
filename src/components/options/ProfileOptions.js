import React, { Component } from 'react'
import { Form, Dropdown, Segment, Icon } from 'semantic-ui-react'
import { translate } from 'react-i18next'
import PropTypes from 'prop-types'

class ImageOptions extends Component {
    constructor(props) {
        super(props)
        this.state = {
            imageType: [{type: 'imgtype', value: 0, text: 'random'},
                        {type: 'imgtype', value: 1, text: props.t("OPT_FRM_BLACKANDWHITE")},
                        {type: 'imgtype', value: 2, text: props.t("OPT_FRM_COLOR")}],
            checkbox: []
        }
    }

    componentDidMount() {
        let localCheckbox = [{checked: this.props.queryParams.preload_private, text: this.props.t("OPT_LBL_QUERYPARAM_PRIVATE"), param: 'preload_private'},
                    {checked: this.props.queryParams.preload_team, text: this.props.t("OPT_LBL_QUERYPARAM_TEAM"), param: 'preload_team'},
                    {checked: this.props.queryParams.preload_community, text: this.props.t("OPT_LBL_QUERYPARAM_COMMUNITY"), param: 'preload_community'},
                    {checked: this.props.queryParams.global, text: this.props.t("OPT_LBL_QUERYPARAM_GLOBAL"), param: 'global'}]
        this.setState({checkbox: localCheckbox})
    }

    changePriority(item, direction, event) {
        let localOrder = this.props.order
        let currentIndex = localOrder.findIndex(index => index === item)
        let tmp
        if (direction === 'up' && currentIndex !== 0) {
            tmp = localOrder[currentIndex-1]
            localOrder[currentIndex-1] = localOrder[currentIndex]
            localOrder[currentIndex] = tmp
        } else if (direction === 'down' && currentIndex !== localOrder.length-1) {
            tmp = localOrder[currentIndex+1]
            localOrder[currentIndex+1] = localOrder[currentIndex]
            localOrder[currentIndex] = tmp
        }
        this.setState({order: localOrder})
        this.props.changeOrder(localOrder)
    }

    onChangeDropdown(event, data) {
        this.props.changeOtions(data)
    }

    onChangeCheckbox(query_param, event, data) {
        this.props.changeQueryParams(query_param)
    }

    componentDidUpdate(prevProps) {
        if (prevProps.queryParams !== this.props.queryParams) {
            this.componentDidMount()
        }
    }

    render() {
        const { t } = this.props

        let priorityList = this.props.order
        priorityList = priorityList.map((item, index) => {
            return (
                <Segment key={index}>
                    <Icon name='arrow circle down' size='large' className='icon-pointer'
                        onClick={this.changePriority.bind(this, item, 'down')}/>
                    <Icon name='arrow circle up' size='large' className='icon-pointer'
                        onClick={this.changePriority.bind(this, item, 'up')}/>
                    {item.text}
                </Segment>
            )
        })

        let query_params = this.state.checkbox.slice()
        query_params = query_params.map((item, index) => {
            return (
                <Form.Checkbox toggle
                    key={index}
                    onChange={this.onChangeCheckbox.bind(this, item.param)}
                    label={item.text}
                    checked={item.checked}
                />
            )
        })

        return (
            <div>
                <Segment.Group horizontal>
                    <Segment>
                        <Form>
                            <Form.Field width={4}>
                                <label> {t("OPT_LBL_IMAGECOLORPREF")} </label>
                                <Dropdown placeholder='Select' selection
                                    options={this.state.imageType}
                                    onChange={this.onChangeDropdown.bind(this)}
                                    defaultValue={this.props.imgType}
                                />
                            </Form.Field>
                            <Form.Field width={4}>
                                <label> {t("OPT_LBL_IMAGETYPEPREF")} </label>
                                <Dropdown placeholder='Select' selection
                                    options={this.props.SymStyle}
                                    onChange={this.onChangeDropdown.bind(this)}
                                    defaultValue={this.props.selectedSymStyle}
                                />
                            </Form.Field>
                        </Form>
                    </Segment>
                    <Segment>
                        <Segment.Group piled>
                            {priorityList}
                        </Segment.Group>
                    </Segment>
                </Segment.Group>
                <Segment>
                    <Form>
                        <Form.Field>
                            <label> {t("OPT_LBL_QUERYPARAM_SYMBOL")} </label>
                            <br />
                            {query_params}
                        </Form.Field>
                    </Form>
                </Segment>
            </div>
        )
    }
}

ImageOptions.propTypes = {
    changeOtions: PropTypes.func.isRequired,
    changeOrder: PropTypes.func.isRequired,
    changeQueryParams: PropTypes.func.isRequired,
    order: PropTypes.array.isRequired,
    imgType: PropTypes.number.isRequired,
    SymStyle: PropTypes.array.isRequired,
    selectedSymStyle: PropTypes.number.isRequired,
    queryParams: PropTypes.object.isRequired,
}

export default translate('translations')(ImageOptions)
